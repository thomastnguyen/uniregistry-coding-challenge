import React from "react";

class Domain extends React.Component {
  onSubmit(e) {
    const {
      id,
      onDomainSave,
    } = this.props;

    e.preventDefault();

    const domain = {
      id,
      domain: this.domain.value,
      registrant_email: this.email.value,
      price: Number(this.price.value),
    };

    onDomainSave && onDomainSave(e, domain);
  }

  render() {
    const {
      domain,
      registrant_email,
      price,
    } = this.props;

    return (
      <form className="form-horizontal">
        <div className="form-group">
          <label className="control-label col-xs-3">
            Domain name
          </label>
          <div className="col-xs-9">
            <input
              type="text"
              className="form-control"
              ref={node => this.domain = node}
              defaultValue={domain}
            />
          </div>
        </div>
        <div className="form-group">
          <label className="control-label col-xs-3">
            Registrant Email
          </label>
          <div className="col-xs-9">
            <input
              type="text"
              className="form-control"
              ref={node => this.email = node}
              defaultValue={registrant_email}
            />
          </div>
        </div>
        <div className="form-group">
          <label className="control-label col-xs-3">
            Price
          </label>
          <div className="col-xs-9">
            <input
              type="text"
              className="form-control"
              ref={node => this.price = node}
              defaultValue={price.toFixed(2)}
            />
          </div>
        </div>
        <div className="form-group">
          <div className="col-xs-offset-3">
            <button className="btn btn-success" onClick={this.onSubmit.bind(this)}>
              Save changes
            </button>
          </div>
        </div>
      </form>
    );
  }
}

Domain.propTypes = {
  id: React.PropTypes.number.isRequired,
  domain: React.PropTypes.string.isRequired,
  registrant_email: React.PropTypes.string.isRequired,
  price: React.PropTypes.number.isRequired,
  onDomainSave: React.PropTypes.func,
};

export default Domain;