import React from "react";

import {
  getJSON,
} from "jquery";

import Domains from "../domains";
import Domain from "../domain";

const extensions = [
  "cars",
  "lol",
];

const updateDomains = (domains, extensions) => {
  return domains.map((domain) => {
    return {
      ...domain,
      hasExtension: extensions.some(
        (extension) => domain.domain.endsWith("." + extension)
      ),
    };

  });
};

class Page extends React.Component {
  componentDidMount() {
    getJSON(`./assets/domains.json`, (data) => {
      const domains = updateDomains(data.domains, extensions);

      this.setState({
        domains,
      });
    });
  }

  state = {
    isDomainPage: false,
    domains: []
  };

  onDomainClick(e, id) {
    getJSON(`./assets/${id}.json`, (data) => {
      this.setState({
        ...this.state,
        isDomainPage: true,
        domain: data,
      });
    });
  }

  onDomainSave(e, domain) {
    const domains = this.state.domains.map((d) => {
      if (d.id !== domain.id) {
        return d;
      }

      return domain;
    });

    this.setState({
      domains: updateDomains(domains, extensions),
      isDomainPage: false,
    });
  }

  render() {
    const {
      isDomainPage,
      domains,
      domain,
    } = this.state;

    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-body">
            {
              isDomainPage
                ? <Domain
                    {...domain}
                    onDomainSave={this.onDomainSave.bind(this)}
                />
                : <Domains
                    domains={domains}
                    onDomainClick={this.onDomainClick.bind(this)}
                  />
            }
          </div>
        </div>
      </div>
    );
  };
}

export default Page;