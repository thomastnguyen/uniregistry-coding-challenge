import React from "react";

const Domain = (props) => {
  const {
    id,
    domain,
    price,
    onDomainClick,
    hasExtension,
  } = props;

  const onClick = (e) => {
    e.preventDefault();

    onDomainClick && onDomainClick(e, id);
  };

  return (
    <tr>
      <td>
        <a href="#" onClick={onClick}>
          {domain}
        </a>
      </td>
      <td className="text-center">
        {hasExtension && "\u2713"}
      </td>
      <td className="text-right">
        ${price.toFixed(2)}
      </td>
    </tr>
  );
};

Domain.propTypes = {
  id: React.PropTypes.number.isRequired,
  domain: React.PropTypes.string.isRequired,
  price: React.PropTypes.number.isRequired,
  onDomainClick: React.PropTypes.func,
  hasExtension: React.PropTypes.bool,
};

export default Domain;