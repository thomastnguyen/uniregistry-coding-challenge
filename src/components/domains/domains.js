import React from "react";

import Domain from "./domain";

const Domains = (props) => {
  const {
    domains = [],
    onDomainClick,
  } = props;

  return (
    <table className="table table-striped">
      <thead>
      <tr>
        <th>
          Domain name
        </th>
        <th className="text-center">
          Uniregistry
        </th>
        <th className="text-right">
          Price
        </th>
      </tr>
      </thead>
      <tbody>
      {
        domains.map((domain) => {
          return (
            <Domain {...domain} key={domain.id} onDomainClick={onDomainClick}/>
          );
        })
      }
      </tbody>
    </table>
  );
};

Domains.propTypes = {
  domains: React.PropTypes.array.isRequired,
  onDomainClick: React.PropTypes.func,
};

export default Domains;