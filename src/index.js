import React from "react";
import ReactDOM from "react-dom";

import "../public/assets/bootstrap.min.css";

import Page from "./components/page";

ReactDOM.render(
  <Page/>,
  document.getElementById("root"),
);
